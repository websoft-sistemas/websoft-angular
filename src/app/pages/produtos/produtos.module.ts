import { NgModule } from "@angular/core";
import { ProdutosListComponent } from "./components/produtos-list/produtos-list.component";
import { ProdutosFormComponent } from "./containers/produtos-form/produtos-form.component";
import { ProdutosComponent } from "./containers/produtos.component";


@NgModule({
    declarations:[
        ProdutosComponent,
        ProdutosListComponent,
        ProdutosFormComponent

    ]
})
export class ProdutosModule{

}