import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ProdutosFormComponent } from "./containers/produtos-form/produtos-form.component";
import { ProdutosComponent } from "./containers/produtos.component";

const routes: Routes = [
    {path: '', component: ProdutosComponent},
    {path: 'adicionar', component: ProdutosFormComponent},
    {path: ':id/alterar', component: ProdutosFormComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]

})
export class ProdutosRoutingModule{

}